<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * @internal
 */
final class BenchmarkCommandTest extends KernelTestCase
{
    /**
     * @var string
     */
    protected $resultDir = __DIR__.'/../../benchmark';

    /**
     * @var array
     */
    protected $texts = [
        'html files of 30 pages generated in',
        'Generated 1 times "Hello World" in',
        '1 overlay applied',
        'Concat 6 pages 1 times',
        'pdfs generated with mailid and datamatrix',
        '1 multiple page pdf splitted in single page pdfs in',
        'Rotated multiple page pdf 90° 1 times in',
    ];

    /**
     * @group command
     */
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('pdf:benchmark:start');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'count' => 1,
        ]);

        $output = $commandTester->getDisplay();

        foreach ($this->texts as $text) {
            static::assertContains($text, $output);
        }
    }

    /**
     * @group command
     * @dataProvider getOptionAndExpectedTexts
     *
     * @param mixed $option
     * @param mixed $expectedText
     */
    public function testExecuteOption($option, $expectedText)
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('pdf:benchmark:start');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'count' => 1,
            $option => true,
        ]);

        $output = $commandTester->getDisplay();
        $texts = $this->texts;

        if (null === $expectedText) {
            foreach ($texts as $text) {
                static::assertContains($text, $output);
            }

            return;
        }
        static::assertContains($expectedText, $output);

        $key = array_search($expectedText, $texts, true);
        unset($texts[$key]);

        foreach ($texts as $text) {
            static::assertNotContains($text, $output);
        }
    }

    /**
     * @return array
     */
    public function getOptionAndExpectedTexts()
    {
        return [
            ['--all', null],
            ['--from-html', 'html files of 30 pages generated in'],
            ['--print', 'Generated 1 times "Hello World" in'],
            ['--overlay', '1 overlay applied'],
            ['--merge', 'Concat 6 pages 1 times'],
            ['--images', 'pdfs generated with mailid and datamatrix'],
            ['--split', '1 multiple page pdf splitted in single page pdfs in'],
            ['--rotate', 'Rotated multiple page pdf 90° 1 times in'],
        ];
    }
}
