<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Lib;

use App\Exception\NotImplementedException;
use App\Handler\AbstractHandler;
use App\Lib\Cpdf;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

abstract class AbstractLibTest extends WebTestCase
{
    /**
     * @var Cpdf
     */
    protected $lib;

    /**
     * @var string
     */
    protected $path = __DIR__.'/../../var/cache/test/pdfs';

    protected function setUp()
    {
        $system = new Filesystem();
        $system->mkdir($this->path);
    }

    protected function tearDown()
    {
        $system = new Filesystem();
        $system->remove($this->path);
    }

    /**
     * @group lib
     * @group merge
     */
    public function testMerge()
    {
        $dest = $this->generateFilename();

        $pdfs = [
            __DIR__.'/../Ressources/exemple.pdf',
            __DIR__.'/../Ressources/exemple2.pdf',
        ];

        if (!$this->lib->supports(AbstractHandler::TYPE_MERGE)) {
            self::expectException(NotImplementedException::class);
        }

        $time = $this->lib->merge($dest, $pdfs);

        if (!$this->lib->supports(AbstractHandler::TYPE_MERGE)) {
            return;
        }

        static::assertIsFloat($time);
        static::assertFileExists($dest);
    }

    /**
     * @group lib
     * @group split
     */
    public function testSplit()
    {
        $dest = $this->generateFilename();

        if (!$this->lib->supports(AbstractHandler::TYPE_SPLIT)) {
            self::expectException(NotImplementedException::class);
        }

        $time = $this->lib->split($dest, __DIR__.'/../Ressources/exemple_multiple_page.pdf');

        if (!$this->lib->supports(AbstractHandler::TYPE_SPLIT)) {
            return;
        }

        $finder = new Finder();

        static::assertIsFloat($time);
        static::assertCount(5, $finder->files()->in($this->path));
    }

    /**
     * @group lib
     * @group rotate
     */
    public function testRotate()
    {
        $dest = $this->generateFilename();

        $pdfs = [
            __DIR__.'/../Ressources/exemple.pdf',
            __DIR__.'/../Ressources/exemple2.pdf',
        ];

        if (!$this->lib->supports(AbstractHandler::TYPE_ROTATE)) {
            self::expectException(NotImplementedException::class);
        }

        $time = $this->lib->rotate($dest, $pdfs);

        if (!$this->lib->supports(AbstractHandler::TYPE_ROTATE)) {
            return;
        }

        static::assertIsFloat($time);
        static::assertFileExists($dest);
    }

    /**
     * @group lib
     * @group html
     *
     * @throws NotImplementedException
     */
    public function testHtml()
    {
        $dest = $this->generateFilename();

        if (!$this->lib->supports(AbstractHandler::TYPE_FROM_HTML)) {
            self::expectException(NotImplementedException::class);
        }

        $time = $this->lib->generateFromHtml('<p>Test</p>', $dest);

        if (!$this->lib->supports(AbstractHandler::TYPE_FROM_HTML)) {
            return;
        }

        static::assertIsFloat($time);
        static::assertFileExists($dest);
    }

    /**
     * @group lib
     * @group hello_world
     *
     * @throws NotImplementedException
     * @throws \Exception
     */
    public function testHelloWorld()
    {
        $dest = $this->generateFilename();

        if (!$this->lib->supports(AbstractHandler::TYPE_HELLO_WORLD)) {
            self::expectException(NotImplementedException::class);
        }

        $time = $this->lib->generateHelloWorld($dest);

        if (!$this->lib->supports(AbstractHandler::TYPE_HELLO_WORLD)) {
            return;
        }

        static::assertIsFloat($time);
        static::assertFileExists($dest);
    }

    /**
     * @group lib
     * @group overlay
     *
     * @throws NotImplementedException
     * @throws \Exception
     */
    public function testOverlay()
    {
        $dest = $this->generateFilename();

        if (!$this->lib->supports(AbstractHandler::TYPE_OVERLAY)) {
            self::expectException(NotImplementedException::class);
        }

        $time = $this->lib->overlay(__DIR__.'/../Ressources/exemple.pdf', __DIR__.'/../Ressources/exemple2.pdf', $dest);

        if (!$this->lib->supports(AbstractHandler::TYPE_OVERLAY)) {
            return;
        }

        static::assertIsFloat($time);
        static::assertFileExists($dest);
    }

    /**
     * @group lib
     * @group images
     *
     * @throws NotImplementedException
     * @throws \Exception
     */
    public function testImages()
    {
        $dest = $this->generateFilename();

        if (!$this->lib->supports(AbstractHandler::TYPE_IMAGES)) {
            self::expectException(NotImplementedException::class);
        }

        $images = [
            'datamatrix' => [
                'path' => __DIR__.'/../Ressources/img/datamatrix.png',
                'position' => [
                    'x' => 10,
                    'y' => 10,
                ],
                'sizes' => [
                    'width' => 10,
                    'height' => 10,
                ],
            ],
            'mailid' => [
                'path' => __DIR__.'/../Ressources/img/barcode.gif',
                'position' => [
                    'x' => 100,
                    'y' => 50,
                ],
                'sizes' => [
                    'width' => 50,
                    'height' => 20,
                ],
            ],
        ];

        $time = $this->lib->images($dest, __DIR__.'/../Ressources/exemple2.pdf', $images);

        if (!$this->lib->supports(AbstractHandler::TYPE_IMAGES)) {
            return;
        }

        static::assertIsFloat($time);
        static::assertFileExists($dest);
    }

    protected function generateFilename(): string
    {
        return $this->path.'/'.md5(rand()).'.pdf';
    }
}
