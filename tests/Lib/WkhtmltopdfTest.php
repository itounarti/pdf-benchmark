<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Lib;

use App\Lib\Wkhtmltopdf;

/**
 * @internal
 */
final class WkhtmltopdfTest extends AbstractLibTest
{
    protected function setUp()
    {
        parent::setUp();

        self::bootKernel();

        $twig = self::$container->get('twig');

        $this->lib = new Wkhtmltopdf($twig);
    }
}
