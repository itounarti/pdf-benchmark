<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Handler;

use App\Handler\RotateHandler;

/**
 * @internal
 */
final class RotateHandlerTest extends AbstractHandlerTest
{
    /**
     * @return RotateHandler
     */
    protected function getHandler()
    {
        return new RotateHandler([$this->lib, $this->wrongLib], $this->progressBar, 'test');
    }

    /**
     * @throws \App\Exception\NotImplementedException
     *
     * @return mixed|void
     */
    protected function execute()
    {
        return $this->handler->rotate($this->output, 10);
    }

    protected function getMethodName(): string
    {
        return 'rotate';
    }
}
