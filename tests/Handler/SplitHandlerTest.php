<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Handler;

use App\Handler\SplitHandler;

/**
 * @internal
 */
final class SplitHandlerTest extends AbstractHandlerTest
{
    /**
     * @return SplitHandler
     */
    protected function getHandler()
    {
        return new SplitHandler([$this->lib, $this->wrongLib], $this->progressBar, 'test');
    }

    /**
     * @throws \App\Exception\NotImplementedException
     *
     * @return mixed|void
     */
    protected function execute()
    {
        return $this->handler->split($this->output, 10);
    }

    protected function getMethodName(): string
    {
        return 'split';
    }
}
