<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Handler;

use App\Handler\OverlayHandler;

/**
 * @internal
 */
final class OverlayHandlerTest extends AbstractHandlerTest
{
    /**
     * @return OverlayHandler
     */
    protected function getHandler()
    {
        return new OverlayHandler([$this->lib, $this->wrongLib], $this->progressBar, 'test');
    }

    /**
     * @throws \App\Exception\NotImplementedException
     *
     * @return mixed|void
     */
    protected function execute()
    {
        return $this->handler->overlay($this->output, 10);
    }

    protected function getMethodName(): string
    {
        return 'overlay';
    }
}
