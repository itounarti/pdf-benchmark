<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Handler;

use App\Handler\HelloWorldHandler;

/**
 * @internal
 */
final class HelloWorldHandlerTest extends AbstractHandlerTest
{
    /**
     * @return HelloWorldHandler
     */
    protected function getHandler()
    {
        return new HelloWorldHandler([$this->lib, $this->wrongLib], $this->progressBar, 'test');
    }

    /**
     * @return array|mixed|void
     */
    protected function execute()
    {
        return $this->handler->helloWorld($this->output, 10);
    }

    protected function getMethodName(): string
    {
        return 'generateHelloWorld';
    }
}
