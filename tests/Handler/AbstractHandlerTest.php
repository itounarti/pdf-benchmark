<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Tests\Handler;

use App\Handler\HelloWorldHandler;
use App\Model\LibInterface;
use App\Utils\ProgressBar;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractHandlerTest extends TestCase
{
    /**
     * @var HelloWorldHandler
     */
    protected $handler;

    /**
     * @var OutputInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $output;

    /**
     * @var LibInterface[]|\PHPUnit\Framework\MockObject\MockObject[]
     */
    protected $libs = [];

    /**
     * @var LibInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $lib;

    /**
     * @var LibInterface|\PHPUnit\Framework\MockObject\MockObject
     */
    protected $wrongLib;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject|ProgressBar
     */
    protected $progressBar;

    protected function setUp()
    {
        $this->output = $this->getMockBuilder(OutputInterface::class)
            ->getMock()
        ;

        $this->lib = $this->getMockBuilder(LibInterface::class)
            ->getMock()
        ;

        $this->wrongLib = $this->getMockBuilder(LibInterface::class)
            ->getMock()
        ;

        $this->progressBar = $this->getMockBuilder(ProgressBar::class)
            ->getMock()
        ;

        $this->lib
            ->expects(static::once())
            ->method('supports')
            ->willReturn(true)
        ;

        $this->handler = $this->getHandler();
    }

    /**
     * @group handler
     */
    public function testHandler()
    {
        $this->lib
            ->expects(static::exactly(4))
            ->method('getName')
            ->willReturn('Good Lib')
        ;

        $this->wrongLib
            ->expects(static::once())
            ->method('getName')
            ->willReturn('Bad Lib')
        ;

        $this->wrongLib
            ->expects(static::once())
            ->method('supports')
            ->willReturn(false)
        ;

        $this->assertProgressBar();

        $result = $this->execute();

        static::assertIsArray($result);
    }

    /**
     * @group handler
     */
    public function testHandlerException()
    {
        $this->lib
            ->expects(static::exactly(3))
            ->method('getName')
            ->willReturn('Good Lib')
        ;

        $this->lib
            ->expects(static::once())
            ->method($this->getMethodName())
            ->willThrowException(new \Exception())
        ;

        $this->wrongLib
            ->expects(static::once())
            ->method('getName')
            ->willReturn('Bad Lib')
        ;

        $this->wrongLib
            ->expects(static::once())
            ->method('supports')
            ->willReturn(false)
        ;

        $result = $this->execute();

        static::assertIsArray($result);
    }

    abstract protected function getHandler();

    abstract protected function execute();

    abstract protected function getMethodName(): string;

    protected function assertProgressBar(int $count = 10)
    {
        $this->progressBar
            ->expects(static::once())
            ->method('start')
        ;

        $this->progressBar
            ->expects(static::exactly($count))
            ->method('advance')
        ;

        $this->progressBar
            ->expects(static::once())
            ->method('finish')
        ;
    }
}
