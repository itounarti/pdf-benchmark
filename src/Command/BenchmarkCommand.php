<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Command;

use App\Exception\NotImplementedException;
use App\Handler\HandlerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

class BenchmarkCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'pdf:benchmark:start';

    /**
     * @var string
     */
    private $resultDir;

    /**
     * @var HandlerInterface[]
     */
    private $handlers;

    /**
     * @var array
     */
    private $results = [];

    /**
     * @var Environment
     */
    private $twig;

    /**
     * GeneratorCommand constructor.
     *
     * @param HandlerInterface[]|iterable $handlers
     */
    public function __construct(string $name = null, iterable $handlers, Environment $twig)
    {
        parent::__construct($name);

        foreach ($handlers as $handler) {
            $this->handlers[] = $handler;
        }

        shuffle($this->handlers);
        $this->twig = $twig;

        $this->resultDir = __DIR__.'/../../benchmark';
    }

    protected function configure()
    {
        $this
            ->setDescription('Launches the benchmark.')
            ->addArgument('count', InputArgument::OPTIONAL, 'How much do we generate ?', 100)
            ->addArgument('output', InputArgument::OPTIONAL, 'Name of the result file', 'result.html')
            ->addOption('all', 'a', InputOption::VALUE_NONE, 'Launch all benchmarks (By default)')
            ->addOption('print', 'p', InputOption::VALUE_NONE, 'Launches the "Hello World" pdf generation Benchmark')
            ->addOption('merge', 'm', InputOption::VALUE_NONE, 'Launches benchmark of a merge of 6 pages')
            ->addOption('from-html', 'fh', InputOption::VALUE_NONE, 'Launches benchmark of generation of a 30 page html')
            ->addOption('split', 's', InputOption::VALUE_NONE, 'Launches benchmark of generation of a 30 page html')
            ->addOption('images', 'i', InputOption::VALUE_NONE, 'Launches benchmark of pdf generation with dataMatrix and mailid.')
            ->addOption('overlay', 'o', InputOption::VALUE_NONE, 'Launches benchmark of pdf generation with overlay.')
            ->addOption('rotate', 'r', InputOption::VALUE_NONE, 'Launches benchmark of pdf rotation')
        ;
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->handlers as $handler) {
            $this->perform($input, $output, $handler);
        }

        $system = new Filesystem();
        $system->mkdir($this->resultDir);
        $count = $input->getArgument('count');

        $content = $this->twig->render('results.html.twig', [
            'results' => array_merge(...$this->results),
            'count' => $count,
        ]);

        $filename = $count.'_'.$input->getArgument('output');

        $resultFile = $this->resultDir.'/'.$filename;
        $system->dumpFile($resultFile, $content);

        $output->writeln(sprintf('Results dumped into <info>"%s"</info> ', realpath($resultFile)));

        return 0;
    }

    protected function perform(InputInterface $input, OutputInterface $output, HandlerInterface $handler)
    {
        $count = $input->getArgument('count');
        $all = $this->isAll($input);

        if ($all || $input->getOption('overlay')) {
            try {
                $this->results[] = $handler->overlay($output, $count);
            } catch (NotImplementedException $e) {
            }
        }

        if ($all || $input->getOption('rotate')) {
            try {
                $this->results[] = $handler->rotate($output, $count);
            } catch (NotImplementedException $e) {
            }
        }

        if ($all || $input->getOption('images')) {
            try {
                $this->results[] = $handler->images($output, $count);
            } catch (NotImplementedException $e) {
            }
        }

        if ($all || $input->getOption('print')) {
            try {
                $this->results[] = $handler->helloWorld($output, $count);
            } catch (NotImplementedException $e) {
            }
        }

        if ($all || $input->getOption('merge')) {
            try {
                $this->results[] = $handler->merge($output, $count);
            } catch (NotImplementedException $e) {
            }
        }

        if ($all || $input->getOption('from-html')) {
            try {
                $this->results[] = $handler->html($output, $count);
            } catch (NotImplementedException $e) {
            }
        }

        if ($all || $input->getOption('split')) {
            try {
                $this->results[] = $handler->split($output, $count);
            } catch (NotImplementedException $e) {
            }
        }
    }

    /**
     * @return bool
     */
    private function isAll(InputInterface $input)
    {
        if ($input->getOption('all')) {
            return true;
        }

        if ($input->getOption('print')) {
            return false;
        }

        if ($input->getOption('merge')) {
            return false;
        }

        if ($input->getOption('from-html')) {
            return false;
        }

        if ($input->getOption('split')) {
            return false;
        }

        if ($input->getOption('rotate')) {
            return false;
        }

        if ($input->getOption('images')) {
            return false;
        }

        if ($input->getOption('overlay')) {
            return false;
        }

        return true;
    }
}
