<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Model;

use App\Exception\NotImplementedException;

abstract class AbstractLib implements LibInterface
{
    private $active = true;

    abstract public function getName(): string;

    abstract public function getSupportedTests(): array;

    final public function deActivate()
    {
        $this->active = false;
    }

    public function start(): float
    {
        return microtime(true);
    }

    public function finish(float $start): float
    {
        $end = microtime(true);

        return $end - $start;
    }

    final public function supports(string $type): bool
    {
        if (!$this->active) {
            return false;
        }

        return \in_array($type, $this->getSupportedTests(), true);
    }

    /**
     * @throws \Exception
     *
     * @return string
     */
    public function generateHelloWorld(string $destination): float
    {
        throw new NotImplementedException('Not implemented');
    }

    /**
     * @throws NotImplementedException
     *
     * @return string
     */
    public function generateFromHtml(string $html, string $destination): float
    {
        throw new NotImplementedException('Not implemented');
    }

    /**
     * @throws NotImplementedException
     *
     * @return string
     */
    public function merge(string $destination, array $pdfPaths): float
    {
        throw new NotImplementedException('Not implemented');
    }

    /**
     * @throws NotImplementedException
     *
     * @return string
     */
    public function split(string $destination, string $pdfPath): float
    {
        throw new NotImplementedException('Not implemented');
    }

    /**
     * @throws NotImplementedException
     */
    public function rotate(string $destination, array $pdfs): float
    {
        throw new NotImplementedException('Not implemented');
    }

    /**
     * @throws NotImplementedException
     */
    public function images(string $destination, string $source, array $images): float
    {
        throw new NotImplementedException('Not implemented');
    }

    /**
     * @throws NotImplementedException
     */
    public function overlay(string $pdf, string $overlay, string $destination): float
    {
        throw new NotImplementedException('Not implemented');
    }
}
