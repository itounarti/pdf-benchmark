<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Model;

use App\Exception\NotImplementedException;

interface LibInterface
{
    //- création d'un pdf (page porteuse d'adresse) ==> generateHelloWorld
    //- split
    //- merge

    //- apposition d'un code barre (mailid, datamatrix)
    //- apposition d'un overlay
    //- rotation
    //- correction/inclusion de fonts (à voir si d'autres librairies que ghostscript permettent ça)
    //- extraction de données (on avait déja regardé et on était parti sur setasign)

    public function getName(): string;

    public function supports(string $type): bool;

    public function rotate(string $destination, array $pdfs): float;

    /**
     * @throws NotImplementedException
     *
     * @return string
     */
    public function merge(string $destination, array $pdfPaths): float;

    public function images(string $destination, string $source, array $images): float;

    /**
     * @throws NotImplementedException
     *
     * @return string
     */
    public function split(string $destination, string $pdfPath): float;

    /**
     * @throws NotImplementedException
     *
     * @return string
     */
    public function generateHelloWorld(string $destination): float;

    /**
     * @throws NotImplementedException
     *
     * @return string
     */
    public function generateFromHtml(string $html, string $destination): float;

    public function overlay(string $pdf, string $overlay, string $destination): float;
}
