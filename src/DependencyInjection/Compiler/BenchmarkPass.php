<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ActionHandlerPass.
 */
class BenchmarkPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $taggedServices = $container->findTaggedServiceIds('pdf_lib');
        $libs = $container->getExtensionConfig('benchmark')[0];

        $excluded = $libs['excluded'] ?: [];
        $included = $libs['included'];

        foreach ($excluded as $id) {
            $service = $container->findDefinition($id);
            $service->addMethodCall('deActivate');
        }

        if (!$included || $included === []) {
            return;
        }

        foreach ($taggedServices as $id => $tags) {
            if (\in_array($id, $included, true)) {
                continue;
            }

            $service = $container->findDefinition($id);
            $service->addMethodCall('deActivate');
        }
    }
}
