<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\DependencyInjection\Compiler;

use App\Handler\HandlerInterface;
use App\Model\LibInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ActionHandlerPass.
 */
class GeneratorPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(LibInterface::class)->addTag('pdf_lib');
        $container->registerForAutoconfiguration(HandlerInterface::class)->addTag('benchmark_handler');
    }
}
