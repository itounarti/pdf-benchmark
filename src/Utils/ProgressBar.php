<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Utils;

use Symfony\Component\Console\Helper\ProgressBar as BaseProgressBar;
use  Symfony\Component\Console\Output\OutputInterface;

class ProgressBar
{
    /**
     * @var BaseProgressBar
     */
    private $progressBar;

    public function start(OutputInterface $output, int $count)
    {
        $this->progressBar = new BaseProgressBar($output, $count);
        $this->progressBar->start();
    }

    public function advance(int $step = 1)
    {
        $this->progressBar->advance($step);
    }

    public function finish()
    {
        $this->progressBar->finish();
    }
}
