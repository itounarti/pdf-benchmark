<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Handler;

use App\Exception\NotImplementedException;
use App\Model\LibInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HtmlHandler extends AbstractHandler
{
    /**
     * @return array|mixed|void
     */
    public function html(OutputInterface $output, int $count)
    {
        $output->writeln(sprintf('<info> -------- HTML --------</info>'));

        $result = [];
        foreach ($this->libraries as $library) {
            $generatorName = $library->getName();

            if (!$library->supports(AbstractHandler::TYPE_FROM_HTML)) {
                continue;
            }

            try {
                $output->writeln(sprintf('<info>%s</info>', $generatorName));

                $result[$generatorName] = $this->generateMultipleFromHtml($output, $library, $count);

                $output->writeln(sprintf(' - <info>%s</info> html files of 30 pages generated in <info>%s</info>', $count, $result[$generatorName]));
            } catch (\Exception $e) {
                $output->writeln('');
                $output->writeln(sprintf(' - <error>%s error : %s</error>', $generatorName, $e->getMessage()));
            }
        }

        return [
            AbstractHandler::TYPE_FROM_HTML => $result,
        ];
    }

    /**
     * @throws NotImplementedException
     *
     * @return float|string
     */
    protected function generateMultipleFromHtml(OutputInterface $output, LibInterface $generator, int $count)
    {
        $generatorName = $generator->getName();
        $type = 'multiple_from_html';

        $directory = $this->path.'/'.$generatorName.'/'.$type;

        $this->init($output, $generator, $count, $type);

        $time = 0.0;
        for ($i = 0; $i < $count; ++$i) {
            $filename = $generatorName.'_'.md5(rand()).'.pdf';

            $time += $generator->generateFromHtml(file_get_contents(__DIR__.'/../../templates/exemple.html'), $directory.'/'.$filename);
            $this->progressBar->advance();
        }

        $this->finish($generator, $type);

        return $time;
    }
}
