<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Handler;

use App\Exception\NotImplementedException;
use App\Model\LibInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImagesHandler extends AbstractHandler
{
    /**
     * @return array|mixed|void
     */
    public function images(OutputInterface $output, int $count)
    {
        $output->writeln(sprintf('<info> -------- DATAMATRIX & MAILID APPOSITION --------</info>'));

        $result = [];
        foreach ($this->libraries as $library) {
            $generatorName = $library->getName();

            if (!$library->supports(AbstractHandler::TYPE_IMAGES)) {
                continue;
            }

            try {
                $output->writeln(sprintf('<info>%s</info>', $generatorName));

                $result[$generatorName] = $this->generateMultipleWithImages($output, $library, $count);

                $output->writeln(sprintf(' - <info>%s</info> pdfs generated with mailid and datamatrix in <info>%s</info>', $count, $result[$generatorName]));
            } catch (\Exception $e) {
                $output->writeln('');
                $output->writeln(sprintf(' - <error>%s error : %s</error>', $generatorName, $e->getMessage()));
            }
        }

        return [
            'datamatrix_and_mailid' => $result,
        ];
    }

    /**
     * @throws NotImplementedException
     *
     * @return float|string
     */
    protected function generateMultipleWithImages(OutputInterface $output, LibInterface $generator, int $count)
    {
        $generatorName = $generator->getName();
        $type = 'multiple_images';

        $directory = $this->path.'/'.$generatorName.'/'.$type;

        $this->init($output, $generator, $count, $type);

        $time = 0.0;
        for ($i = 0; $i < $count; ++$i) {
            $filename = $generatorName.'_'.md5(rand()).'.pdf';

            $images = [
                'datamatrix' => [
                    'path' => __DIR__.'/../../templates/img/datamatrix.png',
                    'position' => [
                        'x' => 10,
                        'y' => 10,
                    ],
                    'sizes' => [
                        'width' => 10,
                        'height' => 10,
                    ],
                ],
                'mailid' => [
                    'path' => __DIR__.'/../../templates/img/barcode.gif',
                    'position' => [
                        'x' => 100,
                        'y' => 50,
                    ],
                    'sizes' => [
                        'width' => 50,
                        'height' => 20,
                    ],
                ],
            ];

            $time += $generator->images($directory.'/'.$filename, __DIR__.'/../../templates/exemple2.pdf', $images);
            $this->progressBar->advance();
        }

        $this->finish($generator, $type);

        return $time;
    }
}
