<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Handler;

use Symfony\Component\Console\Output\OutputInterface;

interface HandlerInterface
{
    /**
     * @return mixed
     */
    public function overlay(OutputInterface $output, int $count);

    /**
     * @return mixed
     */
    public function helloWorld(OutputInterface $output, int $count);

    /**
     * @return mixed
     */
    public function merge(OutputInterface $output, int $count);

    /**
     * @return mixed
     */
    public function html(OutputInterface $output, int $count);

    /**
     * @return mixed
     */
    public function split(OutputInterface $output, int $count);

    /**
     * @return mixed
     */
    public function rotate(OutputInterface $output, int $count);

    /**
     * @return mixed
     */
    public function images(OutputInterface $output, int $count);
}
