<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Handler;

use App\Exception\NotImplementedException;
use App\Model\LibInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OverlayHandler extends AbstractHandler
{
    /**
     * @return array|mixed|void
     */
    public function overlay(OutputInterface $output, int $count)
    {
        $output->writeln(sprintf('<info> -------- OVERLAY --------</info>'));

        $result = [];
        foreach ($this->libraries as $library) {
            $generatorName = $library->getName();

            if (!$library->supports(AbstractHandler::TYPE_OVERLAY)) {
                continue;
            }

            try {
                $output->writeln(sprintf('<info>%s</info>', $generatorName));

                $result[$generatorName] = $this->generateMultipleOverlay($output, $library, $count);

                $output->writeln(sprintf(' - <info>%s</info> overlay applied in <info>%s</info>', $count, $result[$generatorName]));
            } catch (\Exception $e) {
                $output->writeln('');
                $output->writeln(sprintf(' - <error>%s error : %s</error>', $generatorName, $e->getMessage()));
            }
        }

        return [
            AbstractHandler::TYPE_OVERLAY => $result,
        ];
    }

    /**
     * @throws NotImplementedException
     *
     * @return float
     */
    protected function generateMultipleOverlay(OutputInterface $output, LibInterface $generator, int $count)
    {
        $generatorName = $generator->getName();
        $type = 'multiple_overlay';

        $directory = $this->path.'/'.$generatorName.'/'.$type;

        $this->init($output, $generator, $count, $type);

        $time = 0.0;
        for ($i = 0; $i < $count; ++$i) {
            $filename = $generatorName.'_'.md5(rand()).'.pdf';

            $time += $generator->overlay(__DIR__.'/../../templates/exemple2.pdf', __DIR__.'/../../templates/exemple.pdf', $directory.'/'.$filename);
            $this->progressBar->advance();
        }

        $this->finish($generator, $type);

        return $time;
    }
}
