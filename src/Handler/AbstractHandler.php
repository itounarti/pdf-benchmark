<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Handler;

use App\Exception\NotImplementedException;
use App\Model\LibInterface;
use App\Utils\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

abstract class AbstractHandler implements HandlerInterface
{
    //- correction/inclusion de fonts (à voir si d'autres librairies que ghostscript permettent ça)
    //- extraction de données (on avait déja regardé et on était parti sur setasign)

    const TYPE_SPLIT = 'split';
    const TYPE_MERGE = 'merge';
    const TYPE_FROM_HTML = 'from_html';
    const TYPE_HELLO_WORLD = 'hello_world';
    const TYPE_ROTATE = 'rotate';
    const TYPE_IMAGES = 'images';
    const TYPE_OVERLAY = 'overlay';

    /**
     * @var LibInterface[]
     */
    protected $libraries = [];

    /**
     * @var string
     */
    protected $path;

    /**
     * @var ProgressBar
     */
    protected $progressBar;

    /**
     * Handler constructor.
     */
    public function __construct(iterable $libraries, ProgressBar $progressBar, string $env)
    {
        $this->path = __DIR__.'/../../var/cache/'.$env.'/pdfs';
        $this->progressBar = $progressBar;

        foreach ($libraries as $library) {
            $this->libraries[] = $library;
        }
    }

    /**
     * @throws NotImplementedException
     */
    public function helloWorld(OutputInterface $output, int $count)
    {
        throw new NotImplementedException();
    }

    /**
     * @throws NotImplementedException
     */
    public function merge(OutputInterface $output, int $count)
    {
        throw new NotImplementedException();
    }

    /**
     * @throws NotImplementedException
     */
    public function html(OutputInterface $output, int $count)
    {
        throw new NotImplementedException();
    }

    /**
     * @throws NotImplementedException
     */
    public function split(OutputInterface $output, int $count)
    {
        throw new NotImplementedException();
    }

    /**
     * @throws NotImplementedException
     *
     * @return mixed|void
     */
    public function rotate(OutputInterface $output, int $count)
    {
        throw new NotImplementedException();
    }

    /**
     * @throws NotImplementedException
     *
     * @return mixed|void
     */
    public function images(OutputInterface $output, int $count)
    {
        throw new NotImplementedException();
    }

    /**
     * @throws NotImplementedException
     *
     * @return mixed|void
     */
    public function overlay(OutputInterface $output, int $count)
    {
        throw new NotImplementedException();
    }

    protected function init(OutputInterface $output, LibInterface $generator, int $count, string $type)
    {
        $generatorName = $generator->getName();

        $directory = $this->path.'/'.$generatorName.'/'.$type;

        $system = new Filesystem();
        $system->remove($directory);
        $system->mkdir($directory);

        $this->progressBar->start($output, $count);
    }

    /**
     * @param $type
     */
    protected function finish(LibInterface $generator, $type)
    {
        $generatorName = $generator->getName();
        $directory = $this->path.'/'.$generatorName.'/'.$type;

        $this->progressBar->finish();

        $system = new Filesystem();
        $system->remove($directory);
    }
}
