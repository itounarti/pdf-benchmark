<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Handler;

use App\Exception\NotImplementedException;
use App\Model\LibInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RotateHandler extends AbstractHandler
{
    public function rotate(OutputInterface $output, int $count)
    {
        $output->writeln(sprintf('<info> -------- ROTATE --------</info>'));

        $result = [];
        foreach ($this->libraries as $library) {
            $generatorName = $library->getName();

            if (!$library->supports(AbstractHandler::TYPE_ROTATE)) {
                continue;
            }

            try {
                $output->writeln(sprintf('<info>%s</info>', $generatorName));

                $result[$generatorName] = $this->rotateMultiple($output, $library, $count);

                $output->writeln(sprintf(' - Rotated multiple page pdf 90° <info>%s</info> times in <info>%s</info>', $count, $result[$generatorName]));
            } catch (\Exception $e) {
                $output->writeln('');
                $output->writeln(sprintf(' - <error>%s error : %s</error>', $generatorName, $e->getMessage()));
            }
        }

        return [
            AbstractHandler::TYPE_ROTATE => $result,
        ];
    }

    /**
     * @throws NotImplementedException
     *
     * @return float|string
     */
    protected function rotateMultiple(OutputInterface $output, LibInterface $generator, int $count)
    {
        $generatorName = $generator->getName();
        $type = 'multiple_rotate';

        $directory = $this->path.'/'.$generatorName.'/'.$type;

        $this->init($output, $generator, $count, $type);

        $time = 0.0;
        for ($i = 0; $i < $count; ++$i) {
            $filename = $generatorName.'_'.md5(rand()).'.pdf';

            $time += $generator->rotate($directory.'/'.$filename, [__DIR__.'/../../templates/exemple_multiple_page.pdf']);
            $this->progressBar->advance();
        }

        $this->finish($generator, $type);

        return $time;
    }
}
