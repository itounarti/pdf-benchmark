<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;

class Mpdf extends AbstractLib
{
    public function getName(): string
    {
        return 'mPDF';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_FROM_HTML,
            AbstractHandler::TYPE_HELLO_WORLD,
        ];
    }

    /**
     * @throws \Mpdf\MpdfException
     */
    public function generateFromHtml(string $html, string $destination): float
    {
        $start = $this->start();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output($destination, 'F');

        return $this->finish($start);
    }

    /**
     * @throws \Mpdf\MpdfException
     */
    public function generateHelloWorld(string $destination): float
    {
        $start = $this->start();

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML('<h1>Hello world!</h1>');
        $mpdf->Output($destination, 'F');

        return $this->finish($start);
    }
}
