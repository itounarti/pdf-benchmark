<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;
use Symfony\Component\Process\Process;
use Twig\Environment;

class Wkhtmltopdf extends AbstractLib
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * Html2Pdf constructor.
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getName(): string
    {
        return 'Wkhtmltopdf';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_FROM_HTML,
            AbstractHandler::TYPE_HELLO_WORLD,

            AbstractHandler::TYPE_IMAGES,
        ];
    }

    public function generateHelloWorld(string $destination): float
    {
        $htmlPath = str_replace('.pdf', '.html', $destination);

        file_put_contents($htmlPath, '<html><body><p>Hello world</p></body></html>');
        $process = Process::fromShellCommandline(sprintf('wkhtmltopdf %s %s', $htmlPath, $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }

    public function generateFromHtml(string $html, string $destination): float
    {
        $htmlPath = str_replace('.pdf', '.html', $destination);

        file_put_contents($htmlPath, $html);
        $process = Process::fromShellCommandline(sprintf('wkhtmltopdf %s %s', $htmlPath, $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function images(string $destination, string $source, array $images): float
    {
        $htmlPath = str_replace('.pdf', '.html', $destination);

        $html = $this->twig->render('datamatrix_mailid.html.twig', $images);

        file_put_contents($htmlPath, $html);
        $process = Process::fromShellCommandline(sprintf('wkhtmltopdf %s %s', $htmlPath, $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }
}
