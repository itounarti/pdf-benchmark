<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;
use Symfony\Component\Process\Process;

class Pdftk extends AbstractLib
{
    public function getName(): string
    {
        return 'PDFTK';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_MERGE,
            AbstractHandler::TYPE_SPLIT,
            AbstractHandler::TYPE_ROTATE,
            AbstractHandler::TYPE_OVERLAY,
        ];
    }

    public function rotate(string $destination, array $pdfPaths): float
    {
        $realPaths = array_map(function (string $path) {
            return '"'.realpath($path).'"';
        }, $pdfPaths);

        $process = Process::fromShellCommandline(sprintf('pdftk %s cat 1-endeast output %s', implode(' ', $realPaths), $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }

    public function overlay(string $pdf, string $overlay, string $destination): float
    {
        $process = Process::fromShellCommandline(sprintf('pdftk %s stamp %s output %s', $pdf, $overlay, $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }

    public function merge(string $destination, array $pdfPaths): float
    {
        $realPaths = array_map(function (string $path) {
            return '"'.realpath($path).'"';
        }, $pdfPaths);

        $process = Process::fromShellCommandline(sprintf('pdftk %s cat output %s', implode(' ', $realPaths), $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }

    /**
     * @throws \Exception
     */
    public function split(string $destination, string $pdfPath): float
    {
        $start = $this->start();

        $process = Process::fromShellCommandline(sprintf('pdftk %s cat 1 output %s', realpath($pdfPath), str_replace('.pdf', '1.pdf', $destination)));
        $process->mustRun();

        $process = Process::fromShellCommandline(sprintf('pdftk %s cat 2 output %s', realpath($pdfPath), str_replace('.pdf', '2.pdf', $destination)));
        $process->mustRun();

        $process = Process::fromShellCommandline(sprintf('pdftk %s cat 3 output %s', realpath($pdfPath), str_replace('.pdf', '3.pdf', $destination)));
        $process->mustRun();

        $process = Process::fromShellCommandline(sprintf('pdftk %s cat 4 output %s', realpath($pdfPath), str_replace('.pdf', '4.pdf', $destination)));
        $process->mustRun();

        $process = Process::fromShellCommandline(sprintf('pdftk %s cat 5 output %s', realpath($pdfPath), str_replace('.pdf', '5.pdf', $destination)));
        $process->mustRun();

        return $this->finish($start);
    }
}
