<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;
use setasign\Fpdi\Fpdi as BaseFpdi;

class Fpdi extends AbstractLib
{
    public function getName(): string
    {
        return 'Setasign-Fpdi';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_MERGE,
            AbstractHandler::TYPE_HELLO_WORLD,
            AbstractHandler::TYPE_IMAGES,
            AbstractHandler::TYPE_OVERLAY,
        ];
    }

    /**
     * @throws \setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws \setasign\Fpdi\PdfParser\Filter\FilterException
     * @throws \setasign\Fpdi\PdfParser\PdfParserException
     * @throws \setasign\Fpdi\PdfParser\Type\PdfTypeException
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function merge(string $destination, array $pdfPaths): float
    {
        $start = $this->start();

        $pdf = new BaseFpdi();
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 12);

        foreach ($pdfPaths as $i => $pdfPath) {
            $pageCount = $pdf->setSourceFile($pdfPath);
            $pageId = $pdf->ImportPage($pageCount);
            $s = $pdf->getTemplatesize($pageId);
            $pdf->AddPage($s['orientation'], $s);
            $pdf->useImportedPage($pageId);
        }

        $pdf->Output('F', $destination);

        return $this->finish($start);
    }

    /**
     * @throws \setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws \setasign\Fpdi\PdfParser\Filter\FilterException
     * @throws \setasign\Fpdi\PdfParser\PdfParserException
     * @throws \setasign\Fpdi\PdfParser\Type\PdfTypeException
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function overlay(string $pdfName, string $overlay, string $destination): float
    {
        $start = $this->start();

        $pdf = new BaseFpdi();
        $pdf->AddPage();
        $pdf->SetFont('helvetica', 'B', 25);
        $pdf->SetTextColor(255, 0, 0);

        $pdf->setSourceFile($pdfName);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);

        $pdf->Text(40, 120, 'Sample Text over overlay');

        $pdf->Output('F', $destination);

        return $this->finish($start);
    }

    public function generateHelloWorld(string $destination): float
    {
        $start = $this->start();

        $pdf = new BaseFpdi();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 25);
        $pdf->cell(10, 10, 'Hello World');
        $pdf->Output('F', $destination);

        return $this->finish($start);
    }

    /**
     * @throws \setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws \setasign\Fpdi\PdfParser\Filter\FilterException
     * @throws \setasign\Fpdi\PdfParser\PdfParserException
     * @throws \setasign\Fpdi\PdfParser\Type\PdfTypeException
     * @throws \setasign\Fpdi\PdfReader\PdfReaderException
     */
    public function images(string $destination, string $source, array $images): float
    {
        $start = $this->start();

        $pdf = new BaseFpdi();
        $pdf->AddPage();

        $pdf->setSourceFile($source);
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template);

        $dataMatrix = $images['datamatrix'];
        $dataMatrixPosition = $dataMatrix['position'];
        $dataMatrixSizes = $dataMatrix['sizes'];

        $pdf->Image($dataMatrix['path'], $dataMatrixPosition['x'], $dataMatrixPosition['y'], $dataMatrixSizes['width'], $dataMatrixSizes['height']);

        $mailid = $images['mailid'];
        $mailidPosition = $mailid['position'];
        $mailidSizes = $mailid['sizes'];

        $pdf->Image($mailid['path'], $mailidPosition['x'], $mailidPosition['y'], $mailidSizes['width'], $mailidSizes['height']);

        $pdf->Output($destination, 'F');

        return $this->finish($start);
    }
}
