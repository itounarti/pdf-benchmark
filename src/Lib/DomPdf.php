<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;
use Dompdf\Dompdf as BaseDomPdf;

class DomPdf extends AbstractLib
{
    public function getName(): string
    {
        return 'DOMPDF';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_FROM_HTML,
            AbstractHandler::TYPE_HELLO_WORLD,
        ];
    }

    public function generateFromHtml(string $html, string $destination): float
    {
        $start = $this->start();

        $dompdf = new BaseDomPdf();
        $dompdf->loadHtml($html);

        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents($destination, $output);

        return $this->finish($start);
    }

    public function generateHelloWorld(string $destination): float
    {
        $start = $this->start();

        $dompdf = new BaseDomPdf();
        $dompdf->loadHtml('<h1>Hello world!</h1>');

        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $output = $dompdf->output();
        file_put_contents($destination, $output);

        return $this->finish($start);
    }
}
