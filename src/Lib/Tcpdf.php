<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;
use Twig\Environment;

class Tcpdf extends AbstractLib
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * Html2Pdf constructor.
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getName(): string
    {
        return 'TCPDF';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_FROM_HTML,
            AbstractHandler::TYPE_IMAGES,
            AbstractHandler::TYPE_HELLO_WORLD,
        ];
    }

    /**
     * @throws \Exception
     */
    public function generateFromHtml(string $html, string $destination): float
    {
        $start = $this->start();

        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($destination, 'F');

        return $this->finish($start);
    }

    /**
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function images(string $destination, string $source, array $images): float
    {
        $html = $this->twig->render('datamatrix_mailid.html.twig', $images);

        $start = $this->start();

        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->Output($destination, 'F');

        return $this->finish($start);
    }

    /**
     * @throws \Exception
     */
    public function generateHelloWorld(string $destination): float
    {
        $start = $this->start();

        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->AddPage();
        $pdf->writeHTML('Hello World', true, false, true, false, '');
        $pdf->Output($destination, 'F');

        return $this->finish($start);
    }
}
