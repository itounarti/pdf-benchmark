<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;

class SetasignCore extends AbstractLib
{
    public function getName(): string
    {
        return 'SetasignCore';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_ROTATE,
        ];
    }

    /**
     * @throws \SetaPDF_Core_Exception
     * @throws \SetaPDF_Core_SecHandler_Exception
     */
    public function rotate(string $destination, array $pdfPaths): float
    {
        $start = $this->start();

        foreach ($pdfPaths as $pdfPath) {
            $writer = new \SetaPDF_Core_Writer_File($destination);
            $document = \SetaPDF_Core_Document::loadByFilename($pdfPath, $writer);

            $pages = $document->getCatalog()->getPages();
            $pageCount = $pages->count();

            for ($pageNumber = 1; $pageNumber <= $pageCount; ++$pageNumber) {
                $page = $pages->getPage($pageNumber);

                $page->rotateBy(90);
            }

            $document->save()->finish();
        }

        return $this->finish($start);
    }
}
