<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;
use Symfony\Component\Process\Process;

class Cpdf extends AbstractLib
{
    public function getName(): string
    {
        return 'CPDF';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_MERGE,
            AbstractHandler::TYPE_SPLIT,
            AbstractHandler::TYPE_ROTATE,
        ];
    }

    public function merge(string $destination, array $pdfPaths): float
    {
        $realPaths = array_map(function (string $path) {
            return '"'.realpath($path).'"';
        }, $pdfPaths);

        $process = Process::fromShellCommandline(sprintf('cpdf %s -o %s', implode(' ', $realPaths), $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }

    public function rotate(string $destination, array $pdfPaths): float
    {
        $realPaths = array_map(function (string $path) {
            return '"'.realpath($path).'"';
        }, $pdfPaths);

        $process = Process::fromShellCommandline(sprintf('cpdf -rotate-contents 90 %s -o %s', implode(' ', $realPaths), $destination));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }

    public function split(string $destination, string $pdfPath): float
    {
        $process = Process::fromShellCommandline(sprintf('cpdf -split %s -o %s -chunk 1 -gs-malformed', $pdfPath, str_replace('.pdf', '%%%.pdf', $destination)));

        $start = $this->start();
        $process->mustRun();

        return $this->finish($start);
    }
}
