<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;
use Spipu\Html2Pdf\Html2Pdf as BaseHtml2Pdf;
use Twig\Environment;

class Html2Pdf extends AbstractLib
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * Html2Pdf constructor.
     */
    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function getName(): string
    {
        return 'Html2Pdf';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_IMAGES,
            AbstractHandler::TYPE_FROM_HTML,
            AbstractHandler::TYPE_HELLO_WORLD,
        ];
    }

    /**
     * @throws \Exception
     */
    public function generateFromHtml(string $html, string $destination): float
    {
        $start = $this->start();

        $html2pdf = new BaseHtml2Pdf();
        $html2pdf->writeHTML($html);
        $html2pdf->output($destination, 'F');

        return $this->finish($start);
    }

    /**
     * @throws \Exception
     */
    public function generateHelloWorld(string $destination): float
    {
        $start = $this->start();

        $html2pdf = new BaseHtml2Pdf();
        $html2pdf->writeHTML('<h1>HelloWorld</h1>');
        $html2pdf->output($destination, 'F');

        return $this->finish($start);
    }

    /**
     * @throws \Spipu\Html2Pdf\Exception\Html2PdfException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function images(string $destination, string $source, array $images): float
    {
        $html = $this->twig->render('datamatrix_mailid.html.twig', $images);

        $start = $this->start();

        $html2pdf = new BaseHtml2Pdf();
        $html2pdf->writeHTML($html);
        $html2pdf->output($destination, 'F');

        return $this->finish($start);
    }
}
