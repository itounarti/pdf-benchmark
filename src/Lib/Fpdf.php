<?php

/*
 * This file is part of itounarti/pdf-benchmark.
 * (c) Ismaïl TOunarti <ismail.tounarti@gmail.com>
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace App\Lib;

use App\Handler\AbstractHandler;
use App\Model\AbstractLib;

class Fpdf extends AbstractLib
{
    public function getName(): string
    {
        return 'Fpdf';
    }

    public function getSupportedTests(): array
    {
        return [
            AbstractHandler::TYPE_HELLO_WORLD,
            AbstractHandler::TYPE_IMAGES,
        ];
    }

    public function generateHelloWorld(string $destination): float
    {
        $start = $this->start();

        $pdf = new \FPDF();
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 25);
        $pdf->Cell(40, 10, 'Hello World !');
        $pdf->Output('F', $destination);

        return $this->finish($start);
    }

    public function images(string $destination, string $source, array $images): float
    {
        $start = $this->start();

        $pdf = new \FPDF();
        $pdf->AddPage();

        $dataMatrix = $images['datamatrix'];
        $dataMatrixPosition = $dataMatrix['position'];
        $dataMatrixSizes = $dataMatrix['sizes'];

        $pdf->Image($dataMatrix['path'], $dataMatrixPosition['x'], $dataMatrixPosition['y'], $dataMatrixSizes['width'], $dataMatrixSizes['height']);

        $mailid = $images['mailid'];
        $mailidPosition = $mailid['position'];
        $mailidSizes = $mailid['sizes'];

        $pdf->Image($mailid['path'], $mailidPosition['x'], $mailidPosition['y'], $mailidSizes['width'], $mailidSizes['height']);

        $pdf->Output('F', $destination);

        return $this->finish($start);
    }
}
