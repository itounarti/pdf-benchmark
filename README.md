# itounarti/pdf-benchmark

[![licence status](https://img.shields.io/badge/license-MIT-blue)](https://gitlab.com/ismail.tounarti/pdf-benchmark/commits/master)
[![pipeline status](https://gitlab.com/ismail.tounarti/pdf-benchmark/badges/master/pipeline.svg)](https://gitlab.com/ismail.tounarti/pdf-benchmark/commits/master)
[![coverage status](https://gitlab.com/ismail.tounarti/pdf-benchmark/badges/master/coverage.svg)](https://gitlab.com/ismail.tounarti/pdf-benchmark/commits/master)

[[_TOC_]]

## Libs

This projects benchmarks different types of pdf-manipulation libraries.

Those implemented up to now.

* [Fpdf](http://www.fpdf.org/) - Fpdf Library Pdf generator
* [Fpdi](https://www.setasign.com/products/fpdi/about/) -Import pages from existing PDF documents and use them as templates.
* [Html2Pdf](https://html2pdf.fr/) - HTML to PDF converter written in PHP.
* [Setasign-Core](https://www.setasign.com/products/setapdf-core/details/) - Access PDF documents at their lowest level with PHP.
* [Tcpdf](https://tcpdf.org/) - Open Source PHP class for generating PDF documents.
* [mPDF](https://mpdf.github.io/) -  PHP library which generates PDF files from UTF-8 encoded HTML.
* [DOMPdf](https://github.com/dompdf/dompdf) -  HTML to PDF converter.
* [Cpdf](https://community.coherentpdf.com/) - The Coherent PDF Command Line Tools
* [Pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/) -The pdf-toolkif
* [Wkhtmltopdf](https://wkhtmltopdf.org/) - Command line tools to render HTML into PDF and various image formats using the Qt WebKit rendering engine.

## Operations

So, what is benchmarked ?

There are different kinds of operation that are shipped with this project. Those are listed in the options.

## Requirements 

Some command-line tools are also used, they must be installed before.

* [Cpdf](https://community.coherentpdf.com/)
* [Pdftk](https://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/)
* [Wkhtmltopdf](https://wkhtmltopdf.org/) 

The others libs are included with composer.

## Setup

Download the Project and install dependencies 

    git clone https://gitlab.com/itounarti/pdf-benchmark.git
    composer install

## Usage

To start the benchmark :

    bin/console pdf:benchmark:start {count}
    
### Arguments

``count`` : Number of each steps to execute (default = ``100``) 

### Options

* ``-h --help`` : help
* ``--output`` : results filename.
* ``-p --print`` : Generates a Hello World pdf.
* ``-o --overlay`` : Puts an overlay aover another pdf.
* ``-m --merge`` : Merges multiples pdfs together.
* ``-s --split`` : Split a 6 page pdf in multiple single-page pdfs.
* ``-i --images`` : Creates a pdf with A Datamatrix and a Mailid.
* ``-r --rotate`` : Rotates a pdf
* ``--from-html`` : Generates a 30 page pdf.

### Results

Results are dumped into a results_``count``.html file in a ``Benchmark/`` folder in the project.

### Tests

    bin/phpunit

## Config

You can ``include`` or ``exclude`` some libs depending on what you want.

    # config/benchmark.yaml

    benchmark:
        included:
            #- App\Lib\Cpdf
            - App\Lib\DomPdf
        excluded: 
            - App\Lib\Tcpdf
            - App\Lib\Mpdf

## Utils

Some are used to check code-quality and fix them.

### phpcs

Check code quality based on **phpcs.xml** : [phpcs](https://github.com/squizlabs/PHP_CodeSniffer)

The included phpcs.xml file contains standard ``PSR-2`` rules.

    phpcs src/ tests/ --error-severity=1 --warning-severity=8 --extensions=php --standard=phpcs.xml -p

### php-cs-fixer

Fix coding violation based on **.php_cs.dist** : [php-cs-fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer)

    php-cs-fixer fix --config=.php_cs.dist --using-cache=no src/ tests/

## Versions

**Latest stable version :** 1.0.3 - Versions : [click here](https://gitlab.com/itounarti/pdf-benchmark)


## Authors

* **Ismaïl Tounarti**

## License

This project is under ``MIT`` licence - see file [LICENSE.md](LICENSE.md) for more information.

